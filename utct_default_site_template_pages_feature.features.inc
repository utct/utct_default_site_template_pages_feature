<?php
/**
 * @file
 * utct_default_site_template_pages_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function utct_default_site_template_pages_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
