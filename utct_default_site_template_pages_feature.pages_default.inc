<?php
/**
 * @file
 * utct_default_site_template_pages_feature.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function utct_default_site_template_pages_feature_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template__panel_context_d1f8a0bd-e20e-4bbc-ba16-d1d1de139678';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'front',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'omega:simple';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'branding' => NULL,
      'header' => NULL,
      'navigation' => NULL,
      'highlighted' => NULL,
      'help' => NULL,
      'content' => NULL,
      'sidebar_first' => NULL,
      'sidebar_second' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'fc71a80c-717b-4f87-acb5-00d436759fec';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b0df6c11-145e-4446-b517-4f6c3c26ea92';
    $pane->panel = 'content';
    $pane->type = 'pane_messages';
    $pane->subtype = 'pane_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b0df6c11-145e-4446-b517-4f6c3c26ea92';
    $display->content['new-b0df6c11-145e-4446-b517-4f6c3c26ea92'] = $pane;
    $display->panels['content'][0] = 'new-b0df6c11-145e-4446-b517-4f6c3c26ea92';
    $pane = new stdClass();
    $pane->pid = 'new-e7bbcbdb-1c4b-48dc-ae58-e7084be685be';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'shurly-form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e7bbcbdb-1c4b-48dc-ae58-e7084be685be';
    $display->content['new-e7bbcbdb-1c4b-48dc-ae58-e7084be685be'] = $pane;
    $display->panels['content'][1] = 'new-e7bbcbdb-1c4b-48dc-ae58-e7084be685be';
    $pane = new stdClass();
    $pane->pid = 'new-9d0d535c-085f-4f7f-92c6-635a58e6b24d';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'views-custom_shurly_my_urls-block_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9d0d535c-085f-4f7f-92c6-635a58e6b24d';
    $display->content['new-9d0d535c-085f-4f7f-92c6-635a58e6b24d'] = $pane;
    $display->panels['content'][2] = 'new-9d0d535c-085f-4f7f-92c6-635a58e6b24d';
    $pane = new stdClass();
    $pane->pid = 'new-1d7ec1b5-b7e3-4380-b7d9-5d9f7ff40c48';
    $pane->panel = 'highlighted';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1d7ec1b5-b7e3-4380-b7d9-5d9f7ff40c48';
    $display->content['new-1d7ec1b5-b7e3-4380-b7d9-5d9f7ff40c48'] = $pane;
    $display->panels['highlighted'][0] = 'new-1d7ec1b5-b7e3-4380-b7d9-5d9f7ff40c48';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template__panel_context_d1f8a0bd-e20e-4bbc-ba16-d1d1de139678'] = $handler;

  return $export;
}
